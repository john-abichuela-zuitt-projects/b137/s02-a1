package b137.abichuela.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("What is your first name? ");
        String firstName = input.nextLine();
        System.out.println("What is your last name? ");
        String lastName = input.nextLine();
        System.out.println("Enter the year: ");
        int year = input.nextInt();

        System.out.println();

        System.out.println(firstName + " " + lastName);
        if(year % 400 == 0 && year % 100 == 0 && year % 4 == 0) {
            System.out.println(year + " is a leap year");
        }
        else {
            if(year % 4 == 0 && year % 100 != 0){
                System.out.println(year + " is a leap year");
            }
            else {
                System.out.println(year + " is not a leap year");
            }
        }

    }
}
